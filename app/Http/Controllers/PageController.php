<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home()
    {
    	return view('views_app.home');
    }

    public function login()
    {	
    	return view('views_app.login');
    }

    public function register()
    {   
        return view('views_app.register');
    }

    public function shop()
    {
    	return view('views_app.shop');

    }

    public function about()
    { 
        return view('views_app.about');
    }

     public function contact()
    { 
        return view('views_app.contact');
    }

     public function dashboard()
    { 
        return view('views_admin.dashboard');
    }


}
