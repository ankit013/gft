<?php
 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use Validator;

class User extends Model
{
    protected $fillable=[ 'firstname',
    'lastname',
    'email',
    'password',
    'telephone',
    'token'
    ];

    protected $hidden=[ 'password'];

    public function setPasswordAttribute($password)
    {   
        $this->attributes['password'] = bcrypt($password);
    } 

    public function rules(){
    	return $rules = [
        'firstname'=> 'required|min:2|alpha',
        'lastname'=> 'required|min:2|alpha',
        'email' => 'required|email',
        'telephone'=>'required|min:11|numeric',
        'password'=> 'required',
        'confirm_password'=> 'required',
        ];
    }

    public function postUser() {
       
      $user = new User;
             
             
         $validator = Validator::make(Request::all(), $this->rules());
        if($validator->passes())
        {
           $user= User::create(Request::all());
           if($user){
              $user->token=str_random(30);
              $user->save();
           }
           
           
        }
        else{
            return $validator;
        }
    }
}


    