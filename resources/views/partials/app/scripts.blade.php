{!! Html::script('js/jquery.min.js'); !!}
{!! Html::script('js/bootstrap.js'); !!}
{!! Html::script('js/materialize.min.js'); !!}
{!! Html::script('js/custom.js'); !!}
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js'); !!}
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js'); !!}
@yield('page-specific-scripts')
