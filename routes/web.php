<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'home','uses'=>'PageController@home']);
Route::get('/login',['as'=>'login','uses'=>'PageController@login']);
Route::get('/buy-gift-cards',['as'=>'shop','uses'=>'PageController@shop']);
Route::get('/about-us',['as'=>'about','uses'=>'PageController@about']);
Route::get('/contact-us',['as'=>'contact','uses'=>'PageController@contact']);
Route::get('/admin/dashboard',['as'=>'dashboard','uses'=>'PageController@dashboard']);
Route::get('/register',['as'=>'register','uses'=>'PageController@register']);
Route::resource('user','UserController');
Route::post('user/checkmail', 'UserController@checkmail');
