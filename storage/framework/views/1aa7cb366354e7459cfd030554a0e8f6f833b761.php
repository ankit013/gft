<?php $__env->startSection('title'); ?> Buy Gift Cards - GFT.TO <?php $__env->stopSection(); ?>
<?php $__env->startSection('page-content'); ?>
<section class="dir-alp dir-pa-sp-top">
		<div class="container">
			<div class="row">
				<div class="dir-alp-tit">
					<h1>Buy Gift Cards</h1>
					<ol class="breadcrumb">
						<li><a href="/">Home</a> </li>
						<li class="active">Buy Gift Cards</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="dir-alp-con">
					<div class="col-md-3 dir-alp-con-left">
						<div class="dir-alp-con-left-1">
							<h3>Categories</h3> </div>
						<div class="dir-hom-pre dir-alp-left-ner-notb">
							<ul>
								<li>
									<a href="#">
										<div class="list-left-near lln2">
											<h5>Some Category</h5> 
										</div>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="list-left-near lln2">
											<h5>Some Category</h5> 
										</div>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="list-left-near lln2">
											<h5>Some Category</h5> 
										</div>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="list-left-near lln2">
											<h5>Some Category</h5> 
										</div>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="list-left-near lln2">
											<h5>Some Category</h5> 
										</div>
									</a>
								</li>
							</ul>
						</div>
						<div class="dir-alp-l3 dir-alp-l-com">
							<h4>Sub Category Filter</h4>
							<div class="dir-alp-l-com1 dir-alp-p3">
								<form action="#">
									<ul>
										<li>
											<input type="checkbox" id="scf1" />
											<label for="scf1">Hortels & Resorts</label>
										</li>
										<li>
											<input type="checkbox" id="scf2" />
											<label for="scf2">Fitness Care</label>
										</li>
										<li>
											<input type="checkbox" id="scf3" />
											<label for="scf3">Educations</label>
										</li>
										<li>
											<input type="checkbox" id="scf4" />
											<label for="scf4">Property</label>
										</li>
										<li>
											<input type="checkbox" id="scf5" />
											<label for="scf5">Home Services</label>
										</li>
									</ul>
								</form>
							</div>
						</div>
					</div>
					<div class="col-md-9 dir-alp-con-right list-grid-rig-pad">
						<div class="dir-alp-con-right-1">
							<div class="row">	
								<div class="row span-none">
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4">
										<a href="#">
											<div class="list-mig-like-com com-mar-bot-30">
												<div class="list-mig-lc-img"> <img src="images/gift-cards/dell-gift-card.png" alt="">
												</div>
												<div class="list-mig-lc-con">	
													<h5>Holiday Inn Express</h5>
												</div>
											</div>
										</a>
									</div>
								</div>
									<div class="row">
										<ul class="pagination list-pagenat">
											<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a> </li>
											<li class="active"><a href="#!">1</a> </li>
											<li class="waves-effect"><a href="#!">2</a> </li>
											<li class="waves-effect"><a href="#!">3</a> </li>
											<li class="waves-effect"><a href="#!">4</a> </li>
											<li class="waves-effect"><a href="#!">5</a> </li>
											<li class="waves-effect"><a href="#!">6</a> </li>
											<li class="waves-effect"><a href="#!">7</a> </li>
											<li class="waves-effect"><a href="#!">8</a> </li>
											<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a> </li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>