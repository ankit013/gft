<?php $__env->startSection('title'); ?> Login - GFT.TO <?php $__env->stopSection(); ?>
<?php $__env->startSection('page-content'); ?>
<section class="tz-register">
        <div class="tz-regi-form">
            <h4>Sign In</h4>
            <p>It's free and always will be.</p>
            <form class="col s12">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" class="validate">
                        <label>User Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input type="password" class="validate">
                        <label>Password</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12"> <i class="waves-effect waves-light btn-large full-btn waves-input-wrapper" style=""><input type="submit" value="submit" class="waves-button-input"></i> </div>
                </div>
            </form>
            <p><a href="forgot-pass.html">forgot password</a> | Are you a new user ? <a href="<?php echo e(URL::route('register')); ?>">Register</a> </p>
            <div class="soc-login">
                <h4>Sign in using</h4>
                <ul>
                    <li><a href="#"><i class="fa fa-facebook fb1"></i> Facebook</a> </li>
                    <li><a href="#"><i class="fa fa-twitter tw1"></i> Twitter</a> </li>
                    <li><a href="#"><i class="fa fa-google-plus gp1"></i> Google</a> </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="web-app com-padd">
        <div class="container">
            <div class="row">
                <div class="col-md-6 web-app-img"> <img src="images/mobile.png" alt="" /> </div>
                <div class="col-md-6 web-app-con">
                    <h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
                    <ul>
                        <li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
                        <li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
                    </ul> <span>We'll send you a link, open it on your phone to download the app</span>
                    <form>
                        <ul>
                            <li>
                                <input type="text" placeholder="+01" /> </li>
                            <li>
                                <input type="number" placeholder="Enter mobile number" /> </li>
                            <li>
                                <input type="submit" value="Get App Link" /> </li>
                        </ul>
                    </form>
                    <a href="#"><img src="images/android.png" alt="" /> </a>
                    <a href="#"><img src="images/apple.png" alt="" /> </a>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>