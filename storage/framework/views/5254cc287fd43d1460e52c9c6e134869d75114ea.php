<!DOCTYPE html>
<html lang="en">

<head>
    <?php echo $__env->make('partials.app.meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('partials.app.styles', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('partials.app.polyfills', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>

<body>

    <section>
        <div class="v3-top-menu">
            <div class="container">
                <div class="row">
                    <div class="v3-menu">
                        <div class="v3-m-1">
                            <a href="main.html"><img src="images/logo_gft.png" alt=""> </a>
                        </div>
                        <div class="v3-m-2">
                            <ul>
                                <li><a href="<?php echo e(URL::route('home')); ?>">Home</a></li>
                                <li><a href="<?php echo e(URL::route('about')); ?>">About</a></li>
                                <li><a href="<?php echo e(URL::route('shop')); ?>">Buy Gift Cards</a></li>
                                <li><a href='#'>Sell Gift Cards</a></li>
                                <li><a href="<?php echo e(URL::route('contact')); ?>">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="v3-m-3">
                            <div class="v3-top-ri">
                                <ul>
                                    <li><a href="<?php echo e(URL::route('login')); ?>" class="v3-menu-sign"><i class="fa fa-user"></i> Sign In / Register</a> </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="v3-mob-top-menu">
            <div class="container">
                <div class="row">
                    <div class="v3-mob-menu">
                        <div class="v3-mob-m-1">
                            <a href="main.html"><img src="images/logo_gft.png" alt=""> </a>
                        </div>
                        <div class="v3-mob-m-2">
                            <div class="v3-top-ri">
                                <ul>
                                    <li><a href="<?php echo e(URL::route('login')); ?>" class="v3-menu-sign"><i class="fa fa-user"></i> Sign In / Register</a> </li>
                                    <li><a href="#" class="ts-menu-5" id="v3-mob-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i>Menu</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mob-right-nav" data-wow-duration="0.5s">
            <div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
            <ul class="mob-menu-icon">
               <li><a href="<?php echo e(URL::route('home')); ?>">Home</a></li>
               <li><a href="<?php echo e(URL::route('about')); ?>">About</a></li>
               <li><a href="<?php echo e(URL::route('shop')); ?>">Buy Gift Cards</a></li>
               <li><a href='#'>Sell Gift Cards</a></li>
               <li><a href="<?php echo e(URL::route('contact')); ?>">Contact Us</a></li>
           </ul>
           
       </div>
   </section>
   
   <?php echo $__env->yieldContent('page-content'); ?>

   <footer id="colophon" class="site-footer clearfix">
    <div id="quaternary" class="sidebar-container " role="complementary">
        <div class="sidebar-inner">
            <div class="widget-area clearfix">
                <div id="azh_widget-2" class="widget widget_azh_widget">
                    <div data-section="section">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-4 col-md-3 foot-logo"> <img src="images/logo_foot.png" alt="logo">
                                    <p class="hasimg">Worlds's No. 1 Local Business Directory Website.</p>
                                    <p class="hasimg">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
                                </div>
                                <div class="col-sm-4 col-md-3">
                                    <h4>Support & Help</h4>
                                    <ul class="two-columns">
                                        <li> <a href="advertise.html">Advertise us</a> </li>
                                        <li> <a href="about-us.html">About Us</a> </li>
                                        <li> <a href="customer-reviews.html">Review</a> </li>
                                        <li> <a href="how-it-work.html">How it works </a> </li>
                                        <li> <a href="add-listing.html">Add Business</a> </li>
                                        <li> <a href="#!">Register</a> </li>
                                        <li> <a href="#!">Login</a> </li>
                                        <li> <a href="#!">Quick Enquiry</a> </li>
                                        <li> <a href="#!">Ratings </a> </li>
                                        <li> <a href="trendings.html">Top Trends</a> </li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 col-md-3">
                                    <h4>Popular Services</h4>
                                    <ul class="two-columns">
                                        <li> <a href="#!">Hotels</a> </li>
                                        <li> <a href="#!">Hospitals</a> </li>
                                        <li> <a href="#!">Transportation</a> </li>
                                        <li> <a href="#!">Real Estates </a> </li>
                                        <li> <a href="#!">Automobiles</a> </li>
                                        <li> <a href="#!">Resorts</a> </li>
                                        <li> <a href="#!">Education</a> </li>
                                        <li> <a href="#!">Sports Events</a> </li>
                                        <li> <a href="#!">Web Services </a> </li>
                                        <li> <a href="#!">Skin Care</a> </li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 col-md-3">
                                    <h4>Cities Covered</h4>
                                    <ul class="two-columns">
                                        <li> <a href="#!">Atlanta</a> </li>
                                        <li> <a href="#!">Austin</a> </li>
                                        <li> <a href="#!">Baltimore</a> </li>
                                        <li> <a href="#!">Boston </a> </li>
                                        <li> <a href="#!">Chicago</a> </li>
                                        <li> <a href="#!">Indianapolis</a> </li>
                                        <li> <a href="#!">Las Vegas</a> </li>
                                        <li> <a href="#!">Los Angeles</a> </li>
                                        <li> <a href="#!">Louisville </a> </li>
                                        <li> <a href="#!">Houston</a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-section="section" class="foot-sec2">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h4>Payment Options</h4>
                                    <p class="hasimg"> <img src="images/payment.png" alt="payment"> </p>
                                </div>
                                <div class="col-sm-4">
                                    <h4>Address</h4>
                                    <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A. Landmark : Next To Airport</p>
                                    <p> <span class="strong">Phone: </span> <span class="highlighted">+01 1245 2541</span> </p>
                                </div>
                                <div class="col-sm-5 foot-social">
                                    <h4>Follow with us</h4>
                                    <p>Join the thousands of other There are many variations of passages of Lorem Ipsum available</p>
                                    <ul>
                                        <li><a href="#!"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                        <li><a href="#!"><i class="fa fa-google-plus" aria-hidden="true"></i></a> </li>
                                        <li><a href="#!"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                        <li><a href="#!"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
                                        <li><a href="#!"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                        <li><a href="#!"><i class="fa fa-whatsapp" aria-hidden="true"></i></a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</footer>

<section class="copy">
    <div class="container">
        <p>copyrights © 2017 RN53Themes.net. &nbsp;&nbsp;All rights reserved. </p>
    </div>
</section>

<section>

    <div class="modal fade dir-pop-com" id="list-quo" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header dir-pop-head">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Get a Quotes</h4>

                </div>
                <div class="modal-body dir-pop-body">
                    <form method="post" class="form-horizontal">

                        <div class="form-group has-feedback ak-field">
                            <label class="col-md-4 control-label">Full Name *</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="fname" placeholder="" required> </div>
                            </div>

                            <div class="form-group has-feedback ak-field">
                                <label class="col-md-4 control-label">Mobile</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="mobile" placeholder=""> </div>
                                </div>

                                <div class="form-group has-feedback ak-field">
                                    <label class="col-md-4 control-label">Email</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="email" placeholder=""> </div>
                                    </div>

                                    <div class="form-group has-feedback ak-field">
                                        <label class="col-md-4 control-label">Message</label>
                                        <div class="col-md-8 get-quo">
                                            <textarea class="form-control"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback ak-field">
                                        <div class="col-md-6 col-md-offset-4">
                                            <input type="submit" value="SUBMIT" class="pop-btn"> </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <?php echo $__env->make('partials.app.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </body>

            </html>