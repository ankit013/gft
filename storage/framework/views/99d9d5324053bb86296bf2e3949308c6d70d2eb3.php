<?php $__env->startSection('title'); ?> Login - GFT.TO <?php $__env->stopSection(); ?>
<?php $__env->startSection('page-content'); ?>

<section>
		<div class="tz">
			<!--LEFT SECTION-->
			<div class="tz-l">
				<div class="tz-l-1">
					<ul>
						<li><img src="images/db-profile.jpg" alt=""> </li>
						<li><span>80%</span> profile compl</li>
						<li><span>18</span> Notifications</li>
					</ul>
				</div>
				<div class="tz-l-2">
					<ul>
						<li>
							<a href="dashboard.html" class="tz-lma"><img src="images/icon/dbl1.png" alt=""> My Dashboard</a>
						</li>
						<li>
							<a href="db-all-listing.html"><img src="images/icon/dbl2.png" alt=""> All Listing</a>
						</li>
						<li>
							<a href="db-listing-add.html"><img src="images/icon/dbl3.png" alt=""> Add New Listing</a>
						</li>
						<li>
							<a href="db-message.html"><img src="images/icon/dbl14.png" alt=""> Messages(12)</a>
						</li>
						<li>
							<a href="db-review.html"><img src="images/icon/dbl13.png" alt=""> Reviews(05)</a>
						</li>
						<li>
							<a href="db-my-profile.html"><img src="images/icon/dbl6.png" alt=""> My Profile</a>
						</li>
						<li>
							<a href="db-post-ads.html"><img src="images/icon/dbl11.png" alt=""> Ad Summary</a>
						</li>
						<li>
							<a href="db-payment.html"><img src="images/icon/dbl9.png" alt=""> Payments</a>
						</li>
						<li>
							<a href="db-invoice-all.html"><img src="images/icon/db21.png" alt=""> Invoice</a>
						</li>						
						<li>
							<a href="db-claim.html"><img src="images/icon/dbl7.png" alt=""> Claim &amp; Refund</a>
						</li>
						<li>
							<a href="db-setting.html"><img src="images/icon/dbl210.png" alt=""> Setting</a>
						</li>
						<li>
							<a href="#!"><img src="images/icon/dbl12.png" alt=""> Log Out</a>
						</li>
					</ul>
				</div>
			</div>
			<!--CENTER SECTION-->
			<div class="tz-2">
				<div class="tz-2-com tz-2-main">
					<h4>Manage Booking</h4>
					<div class="tz-2-main-com">
						<div class="tz-2-main-1">
							<div class="tz-2-main-2"> <img src="images/icon/d1.png" alt=""><span>All Listings</span>
								<p>All the Lorem Ipsum generators on the</p>
								<h2>04</h2> </div>
						</div>
						<div class="tz-2-main-1">
							<div class="tz-2-main-2"> <img src="images/icon/d2.png" alt=""><span>Review</span>
								<p>All the Lorem Ipsum generators on the</p>
								<h2>69</h2> </div>
						</div>
						<div class="tz-2-main-1">
							<div class="tz-2-main-2"> <img src="images/icon/d3.png" alt=""><span>Messages</span>
								<p>All the Lorem Ipsum generators on the</p>
								<h2>53</h2> </div>
						</div>
					</div>
					<div class="db-list-com tz-db-table">
						<div class="ds-boar-title">
							<h2>Listings</h2>
							<p>All the Lorem Ipsum generators on the All the Lorem Ipsum generators on the</p>
						</div>
						<table class="responsive-table bordered">
							<thead>
								<tr>
									<th>Listing Name</th>
									<th>Date</th>
									<th>Rating</th>
									<th>Status</th>
									<th>Edit</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Taj Luxury Hotel &amp; Resorts</td>
									<td>12 May 2017</td>
									<td><span class="db-list-rat">4.2</span>
									</td>
									<td><span class="db-list-ststus">Active</span>
									</td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a>
									</td>
								</tr>
								<tr>
									<td>Joney Health and Fitness</td>
									<td>12 May 2017</td>
									<td><span class="db-list-rat">3.4</span>
									</td>
									<td><span class="db-list-ststus-na">Non-Active</span>
									</td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a>
									</td>
								</tr>
								<tr>
									<td>Effi Furniture Dealers</td>
									<td>12 May 2017</td>
									<td><span class="db-list-rat">5.0</span>
									</td>
									<td><span class="db-list-ststus-na">Non-Active</span>
									</td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
				</div>
			</div>
		</div>
	</section>
	<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>