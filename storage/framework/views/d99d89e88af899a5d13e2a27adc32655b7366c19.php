<?php $__env->startSection('title'); ?> Home - GFT.TO <?php $__env->stopSection(); ?>
<?php $__env->startSection('page-content'); ?>
<section id="background1" class="dir1-home-head">
    <div class="container dir-ho-t-sp">
        <div class="row">
            <div class="dir-hr1">
                <div class="dir-ho-t-tit">
                    <h1>Connect with the right Service Experts</h1> </div>
                <div class="home-tab-search">
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <div class="dir-ho-t2l dir-v2-ho-t2l">
                                <form>
                                    <ul>
                                        <li>
                                            <input type="text" class="sea-drop" placeholder="Search your nearby listings and more">
                                            <div class="sea-drop-com sea-drop-1 sea-v2-drop-1">
                                                <ul>
                                                    <li>
                                                        <a href="list.html"><img src="images/menu/1.png" alt="">Property Management Services</a>
                                                    </li>
                                                    <li>
                                                        <a href="list.html"><img src="images/menu/4.png" alt="">Hotel and Resorts</a>
                                                    </li>
                                                    <li>
                                                        <a href="list.html"><img src="images/menu/2.png" alt="">Education and Traninings</a>
                                                    </li>
                                                    <li>
                                                        <a href="list.html"><img src="images/menu/3.png" alt="">Internet Service Providers</a>
                                                    </li>
                                                    <li>
                                                        <a href="list.html"><img src="images/menu/5.png" alt="">Computer Repair & Services</a>
                                                    </li>
                                                    <li>
                                                        <a href="list.html"><img src="images/menu/6.png" alt="">Coaching & Tuitions</a>
                                                    </li>
                                                    <li>
                                                        <a href="list.html"><img src="images/menu/1.png" alt="">Job Training</a>
                                                    </li>
                                                    <li>
                                                        <a href="list.html"><img src="images/menu/7.png" alt="">Skin Care & Treatment</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <input type="submit" value="Search"> </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <div class="dir-ho-t2l">
                                <form>
                                    <ul>
                                        <li>
                                            <input type="text" placeholder="Enter a nearby business"> </li>
                                        <li>
                                            <input type="text" placeholder="Londan"> </li>
                                        <li>
                                            <input type="submit" value="Search"> </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <div class="dir-ho-t2l">
                                <form>
                                    <ul>
                                        <li>
                                            <input type="text" placeholder="Find your property for buy and sell"> </li>
                                        <li>
                                            <input type="text" placeholder="Londan"> </li>
                                        <li>
                                            <input type="submit" value="Search"> </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <div class="dir-ho-t2l">
                                <form>
                                    <ul>
                                        <li>
                                            <input type="text" placeholder="Enter a hotel, resorts, events and more"> </li>
                                        <li>
                                            <input type="text" placeholder="Londan"> </li>
                                        <li>
                                            <input type="submit" value="Search"> </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cat-v2-hom com-padd mar-bot-red-m30">
    <div class="container">
        <div class="row">
            <div class="com-title">
                <h2>Find your <span>Services</span></h2>
                <p>Explore some of the best business from around the world from our partners and friends.</p>
            </div>
            <div class="cat-v2-hom-list">
                <ul>
                    <li>
                        <a href="#"><img src="images/icon/hcat1.png" alt=""> Hospitals</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat2.png" alt=""> Hotel & Resort</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat3.png" alt=""> Events</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat4.png" alt=""> Wedding Halls</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat5.png" alt=""> Shops</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat6.png" alt=""> Fitness & Gym</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat7.png" alt=""> Sports</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat8.png" alt=""> Education</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat9.png" alt=""> Electricals</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat10.png" alt=""> Automobiles</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat11.png" alt=""> Real Estates</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat12.png" alt=""> Import & Export</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat13.png" alt=""> Interior Design</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat14.png" alt=""> Software Solutions</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/hcat15.png" alt=""> Yoga Training</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="com-padd com-padd-redu-top">
    <div class="container">
        <div class="row">
            <div class="com-title">
                <h2>Explore your <span>City Listings</span></h2>
                <p>Explore some of the best business from around the world from our partners and friends.</p>
            </div>
            <div class="col-md-6">
                <a href="list-lead.html">
                    <div class="list-mig-like-com">
                        <div class="list-mig-lc-img"> <img src="images/listing/home.jpg" alt="" /> </div>
                        <div class="list-mig-lc-con">
                            <div class="list-rat-ch list-room-rati"> <span>4.0</span> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> </div>
                            <h5>United States</h5>
                            <p>21 Cities . 2045 Listings . 3648 Users</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="list-lead.html">
                    <div class="list-mig-like-com">
                        <div class="list-mig-lc-img"> <img src="images/listing/home2.jpg" alt="" /> </div>
                        <div class="list-mig-lc-con list-mig-lc-con2">
                            <h5>United Kingdom</h5>
                            <p>18 Cities . 1454 Listings</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="list-lead.html">
                    <div class="list-mig-like-com">
                        <div class="list-mig-lc-img"> <img src="images/listing/home3.jpg" alt="" /> </div>
                        <div class="list-mig-lc-con list-mig-lc-con2">
                            <h5>Australia</h5>
                            <p>14 Cities . 1895 Listings</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="list-lead.html">
                    <div class="list-mig-like-com">
                        <div class="list-mig-lc-img"> <img src="images/listing/home4.jpg" alt="" /> </div>
                        <div class="list-mig-lc-con list-mig-lc-con2">
                            <h5>Germany</h5>
                            <p>12 Cities . 1260 Listings</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a href="list-lead.html">
                    <div class="list-mig-like-com">
                        <div class="list-mig-lc-img"> <img src="images/listing/home1.jpg" alt="" /> </div>
                        <div class="list-mig-lc-con list-mig-lc-con2">
                            <h5>India</h5>
                            <p>24 Cities . 4152 Listings</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="com-padd home-dis">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><span>30% Off</span> Promote Your Business with us <a href="price.html">Add My Business</a></h2> </div>
        </div>
    </div>
</section>

<section class="com-padd com-padd-redu-bot1">
    <div class="container dir-hom-pre-tit">
        <div class="row">
            <div class="com-title">
                <h2>Top Trendings for <span>your City</span></h2>
                <p>Explore some of the best tips from around the world from our partners and friends.</p>
            </div>
            <div class="col-md-6">
                <div>

                    <div class="home-list-pop">

                        <div class="col-md-3"> <img src="images/services/tr1.jpg" alt="" /> </div>

                        <div class="col-md-9 home-list-pop-desc"> <a href="automobile-listing-details.html"><h3>Import Motor America</h3></a>
                            <h4>Express Avenue Mall, Santa Monica</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span class="home-list-pop-rat">4.2</span>
                            <div class="hom-list-share">
                                <ul>
                                    <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a> </li>
                                    <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a> </li>
                                    <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a> </li>
                                    <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="home-list-pop">

                        <div class="col-md-3"> <img src="images/services/tr2.jpg" alt="" /> </div>

                        <div class="col-md-9 home-list-pop-desc"> <a href="property-listing-details.html"><h3>Luxury Property</h3></a>
                            <h4>Express Avenue Mall, New York</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span class="home-list-pop-rat">4.2</span>
                            <div class="hom-list-share">
                                <ul>
                                    <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a> </li>
                                    <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a> </li>
                                    <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a> </li>
                                    <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="home-list-pop">

                        <div class="col-md-3"> <img src="images/services/tr3.jpg" alt="" /> </div>

                        <div class="col-md-9 home-list-pop-desc"> <a href="shop-listing-details.html"><h3>Spicy Supermarket Shop</h3></a>
                            <h4>Express Avenue Mall, Chicago</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span class="home-list-pop-rat">4.2</span>
                            <div class="hom-list-share">
                                <ul>
                                    <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a> </li>
                                    <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a> </li>
                                    <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a> </li>
                                    <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="home-list-pop">

                        <div class="col-md-3"> <img src="images/services/s4.jpg" alt="" /> </div>

                        <div class="col-md-9 home-list-pop-desc"> <a href="list-lead.html"><h3>Packers and Movers</h3></a>
                            <h4>Express Avenue Mall, Toronto</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span class="home-list-pop-rat">4.2</span>
                            <div class="hom-list-share">
                                <ul>
                                    <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a> </li>
                                    <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a> </li>
                                    <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a> </li>
                                    <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div>

                    <div class="home-list-pop">

                        <div class="col-md-3"> <img src="images/services/s5.jpg" alt="" /> </div>

                        <div class="col-md-9 home-list-pop-desc"> <a href="list-lead.html"><h3>Tour and Travels</h3></a>
                            <h4>Express Avenue Mall, Los Angeles</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span class="home-list-pop-rat">4.2</span>
                            <div class="hom-list-share">
                                <ul>
                                    <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a> </li>
                                    <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a> </li>
                                    <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a> </li>
                                    <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="home-list-pop">

                        <div class="col-md-3"> <img src="images/services/s6.jpg" alt="" /> </div>

                        <div class="col-md-9 home-list-pop-desc"> <a href="list-lead.html"><h3>Andru Modular Kitchen</h3></a>
                            <h4>Express Avenue Mall, San Diego</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span class="home-list-pop-rat">4.2</span>
                            <div class="hom-list-share">
                                <ul>
                                    <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a> </li>
                                    <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a> </li>
                                    <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a> </li>
                                    <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="home-list-pop">

                        <div class="col-md-3"> <img src="images/services/s7.jpg" alt="" /> </div>

                        <div class="col-md-9 home-list-pop-desc"> <a href="list-lead.html"><h3>Rute Skin Care & Treatment</h3></a>
                            <h4>Express Avenue Mall, Toronto</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span class="home-list-pop-rat">4.2</span>
                            <div class="hom-list-share">
                                <ul>
                                    <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a> </li>
                                    <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a> </li>
                                    <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a> </li>
                                    <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="home-list-pop mar-bot-red-0">

                        <div class="col-md-3"> <img src="images/services/s8.jpg" alt="" /> </div>

                        <div class="col-md-9 home-list-pop-desc"> <a href="list-lead.html"><h3>Health and Fitness</h3></a>
                            <h4>Express Avenue Mall, San Diego</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A.</p> <span class="home-list-pop-rat">4.2</span>
                            <div class="hom-list-share">
                                <ul>
                                    <li><a href="#!"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a> </li>
                                    <li><a href="#!"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a> </li>
                                    <li><a href="#!"><i class="fa fa-eye" aria-hidden="true"></i> 420</a> </li>
                                    <li><a href="#!"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="com-padd sec-bg-white">
    <div class="container">
        <div class="row">
            <div class="com-title">
                <h2>Create a free <span>Account</span></h2>
                <p>Explore some of the best tips from around the world from our partners and friends.</p>
            </div>
            <div class="col-md-6">
                <div class="hom-cre-acc-left">
                    <h3>A few reasons you’ll love Online <span>Business Directory</span></h3>
                    <p>5 Benefits of Listing Your Business to a Local Online Directory</p>
                    <ul>
                        <li> <img src="images/icon/7.png" alt="">
                            <div>
                                <h5>Enhancing Your Business</h5>
                                <p>Imagine you have made your presence online through a local online directory, but your competitors have..</p>
                            </div>
                        </li>
                        <li> <img src="images/icon/5.png" alt="">
                            <div>
                                <h5>Advertising Your Business</h5>
                                <p>Advertising your business to area specific has many advantages. For local businessmen, it is an opportunity..</p>
                            </div>
                        </li>
                        <li> <img src="images/icon/6.png" alt="">
                            <div>
                                <h5>Develop Brand Image</h5>
                                <p>Your local business too needs brand management and image making. As you know the local market..</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="hom-cre-acc-left hom-cre-acc-right">
                    <form>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="acc-name" type="text" class="validate">
                                <label for="acc-name">Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="acc-mob" type="number" class="validate">
                                <label for="acc-mob">Mobile</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="acc-mail" type="email" class="validate">
                                <label for="acc-mail">Email</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="acc-pass" type="password" class="validate">
                                <label for="acc-pass">Password</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 hom-cr-acc-check">
                                <input type="checkbox" id="test5" />
                                <label for="test5">By signing up, you agree to the Terms and Conditions and Privacy Policy. You also agree to receive product-related emails.</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12"> <a class="waves-effect waves-light btn-large full-btn" href="#!">Submit Now</a> </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>