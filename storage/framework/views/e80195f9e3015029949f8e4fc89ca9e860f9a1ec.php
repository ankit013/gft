<?php $__env->startSection('title'); ?> Register - GFT.TO <?php $__env->stopSection(); ?>
<?php $__env->startSection('page-content'); ?>

<section class="tz-register">
		<div class="tz-regi-form">
			<h4>Create an Account</h4>
			<p>It's free and always will be.</p>
			<?php echo Form::open(array('route' => 'user.store','class' => 'col s12','id' => 'myRegister')); ?>

			
				<div class="row">
					<div class="input-field col m6 s12">
					<?php echo Form::text('firstname', null,  array( 'class'=>'validate')); ?>

						<label class="">First Name</label>
					</div>
					<div class="input-field col m6 s12">
					<?php echo Form::text('lastname', null,  array( 'class'=>'validate')); ?>

						<label>Last Name</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
					<?php echo Form::text('telephone', null,  array( 'class'=>'validate')); ?>

						<label>Mobile</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
					<?php echo Form::email('email', null,  array( 'class'=>'validate', 'id'=>'email')); ?>

						<label>Email</label>
					<p id="email-err"></p>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
					<?php echo Form::password('password', array('class'=>'validate pwd')); ?>

						<label>Password</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
					<?php echo Form::password('confirm_password',array( 'class'=>'validate')); ?>

						<label>Confirm Password</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
					<?php echo Form::submit('submit', array( 'class'=>'btn-large full-btn')); ?>

					 </div>
				</div>
			</form>
			<p>Are you a already member ? <a href="<?php echo e(URL::route('login')); ?>">Click to Login</a> </p>
			<div class="soc-login">
				<h4>Sign in using</h4>
				<ul>
					<li><a href="#"><i class="fa fa-facebook fb1"></i> Facebook</a> </li>
					<li><a href="#"><i class="fa fa-twitter tw1"></i> Twitter</a> </li>
					<li><a href="#"><i class="fa fa-google-plus gp1"></i> Google</a> </li>
				</ul>
			</div>			
		</div>
	</section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-specific-scripts'); ?>
<script type="text/javascript">
$(function() {
	  $("#myRegister").validate({
	  	 errorElement:'p',
    	rules: {
        firstname: {
            required: true,
            minlength: 3
        },
        lastname: {
            required: true,
            minlength: 3
        },
         telephone: {
            required: true,
            minlength: 3
        },
         email: {
            required: true,
            minlength: 3
        },
        password: {
            minlength : 8,
            alphanumeric : true
        },
        confirm_password : {
            minlength : 8,
            alphanumeric : true,
            equalTo : ".pwd"
        }
    },
    submitHandler: function(form) {
     var formData = {
     	firstname : $("input[name=firstname]").val(),
     	lastname : $("input[name=lastname]").val(),
     	telephone : $("input[name=telephone]").val(),
     	email : $("input[name=email]").val(),
     	password : $("input[name=password]").val(),
    	confirm_password : $("input[name=confirm_password]").val(),
     	_token : $("input[name=_token]").val()
     };
     $.ajax({
     	type:"post",
     	url: "user",
     	data: formData,
     	success:function(data){
     		console.log(data);
     	}
     });
    }
  });

});

$('#email').on('keyup keypress blur change', function(event) {
     var emaildata={
        email : $("input[name=email]").val(),
     	_token : $("input[name=_token]").val()
     };

       $.ajax({
     	type:"post",
     	url: "user/checkmail",
     	data: emaildata,
     	success:function(data){
     		if (data!="available") {
     			console.log(data);
               $('#email-err').text("email exists").show();
     		}
     		else{
     			$('#email-err').hide();
     		}
     	}
     });
    });
  


</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>